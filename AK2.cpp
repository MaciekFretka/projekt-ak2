#include<iostream>
#include<cmath>
#include<ctime>
#include <chrono>
#include <thread>
using namespace std;

 
class RNS{
    public:
int m1=71;
int m2=89;
int m3=97;
int x1=0;
int x2=0;
int x3=0;
int k1=0,k2=0,k3=0;
int _m1;
int _m2;
int _m3;

 void IntToRns(int X){
     x1=(X%m1);
     x2=(X%m2);
     x3=(X%m3);
}
int ToInt(){

//cout<<"k1: "<<k1<<" k2: "<<k2<<" k3: "<<k3<<endl;
//cout<<"("<<k1<<"*"<<x1<<"*"<<_m1<<"+"<<k2<<"*"<<x2<<"*"<<_m2<<"+"<<k3<<"*"<<x3<<"*"<<_m3<<")%("<<m1<<"*"<<m2<<"*"<<m3<<")"<<endl;
return (k1*x1*_m1+k2*x2*_m2+k3*x3*_m3)%(m1*m2*m3);
}
void read(){
    cout<<"<"<<x1<<","<<x2<<","<<x3<<"> Base: ("<<m1<<","<<m2<<","<<m3<<")";
}
void calcK(){
_m1=m2*m3;
_m2=m1*m3;
_m3=m1*m2;
bool K1=false,K2=false,K3=false;
bool flag=true;
while(flag){
    if(((k1*_m1)%m1)==1){
        K1=true;
    }else{
        k1++;
    }

    if(((k2*_m2)%m2)==1){
        K2=true;
    }else{
        k2++;
    }

    if(((k3*_m3)%m3)==1){
        K3=true;
    }else{
        k3++;
    }

    if(K1 && K2 && K3) flag=false;

}
}
RNS(int y1,int y2, int y3){
x1=y1;
x2=y2;
x3=y3;

calcK();
}
RNS(int X){
	if(X>(m1*m2*m3) || X<-(m1*m2*m3)){
		cout<<"Wystąpił nadmiar wartości w systmie RNS"<<endl;
	}
IntToRns(X);
calcK();

}
RNS(){

}

};
RNS addRNS(RNS A,RNS B){
RNS s((A.x1+B.x1)%A.m1,(A.x2+B.x2)%A.m2,(A.x3+B.x3)%A.m3);
    
    return s;
 }
 
 RNS subRNS(RNS A,RNS B){
RNS s((A.x1-B.x1)%A.m1,(A.x2-B.x2)%A.m2,(A.x3-B.x3)%A.m3);
    
    return s;
 }
 RNS multiplyRNS(RNS A,RNS B){
     RNS s((A.x1*B.x1)%A.m1,(A.x2*B.x2)%A.m2,(A.x3*B.x3)%A.m3);
    
    return s;
 }
RNS syntaxRNS(RNS A){
int a=A.ToInt();
a>>=8;
RNS s(a);

return s;

}

class SygnalRNS{
public:
RNS dlugosc=RNS(8);
RNS WynikR[8];
RNS WynikI[8];
RNS rzeczywista[8];
RNS urojona[8];
void readsignalRNS(){
	cout<<"Sygnał RNS: "<<endl;
	for(int i=0;i<8;i++){
			cout<<"rzeczywista: ";
			rzeczywista[i].read();
			cout<<" urojona: ";
			urojona[i].read();
			cout<<"W int: ";
			cout<<"rzeczywista: "<<rzeczywista[i].ToInt()<<" urojona: "<<urojona[i].ToInt()<<endl;
	}
}

void fft(){

	RNS Wr[4];
	RNS Wi[4];
	RNS temporaryR;
	RNS temporaryI;
	RNS WynikR[8];
	RNS WynikI[8];
	// Wr[0]=RNS(16384);
	// Wr[1]=RNS(11585);
	// Wr[2]=RNS(0);
	// Wr[3]=RNS(-11585);
	// Wi[0]=RNS(0);
	// Wi[1]=RNS(-11585);
	// Wi[2]=RNS(-16384);
	// Wi[3]=RNS(-11585);
	Wr[0]=RNS(256);
	Wr[1]=RNS(181);
	Wr[2]=RNS(0);
	Wr[3]=RNS(-181);
	Wi[0]=RNS(0);
	Wi[1]=RNS(-181);
	Wi[2]=RNS(-256);
	Wi[3]=RNS(-181);
	
WynikR[0]=rzeczywista[0];
WynikI[0]=urojona[0];
WynikR[1]=rzeczywista[4];
WynikI[1]=urojona[4];
WynikR[2]=rzeczywista[2];
WynikI[2]=urojona[2];
WynikR[3]=rzeczywista[6];
WynikI[3]=urojona[6];
WynikR[4]=rzeczywista[1];
WynikI[4]=urojona[1];
WynikR[5]=rzeczywista[5];
WynikI[5]=urojona[5];
WynikR[6]=rzeczywista[3];
WynikI[6]=urojona[3];
WynikR[7]=rzeczywista[7];
WynikI[7]=urojona[7];
  for(int i=0;i<8;i++)
  {
  	rzeczywista[i]=WynikR[i];
  	urojona[i]=WynikI[i];
  }

  for(int i=1;i<8;i=i+2){
	 
	  temporaryR=rzeczywista[i];
	  rzeczywista[i]=addRNS(multiplyRNS(rzeczywista[i],Wr[0]),multiplyRNS(urojona[i],multiplyRNS(Wi[0],RNS(-1))));
	  
	 rzeczywista[i]=syntaxRNS(rzeczywista[i]);
	 
	  urojona[i]=addRNS(multiplyRNS(urojona[i],Wr[0]),multiplyRNS(temporaryR,Wi[0]));
	  urojona[i]=syntaxRNS(urojona[i]);
  }

    for(int i=0;i<8;i=i+2){
	  temporaryR=addRNS(rzeczywista[i],rzeczywista[i+1]);
	  temporaryI=addRNS(urojona[i],urojona[i+1]);
	  rzeczywista[i+1]=subRNS(rzeczywista[i],rzeczywista[i+1]);
	  urojona[i+1]=subRNS(urojona[i],urojona[i+1]);
	  rzeczywista[i]=temporaryR;
	  urojona[i]=temporaryI;
  }

  for(int i=2;i<8;i=i+4){
	  rzeczywista[i]=multiplyRNS(rzeczywista[i],Wr[0]);
	 rzeczywista[i]=syntaxRNS(rzeczywista[i]);
	  urojona[i]=multiplyRNS(urojona[i],Wr[0]);
	urojona[i]=syntaxRNS(urojona[i]);
	  temporaryR=rzeczywista[i+1];
	  rzeczywista[i+1]=multiplyRNS(urojona[i+1],multiplyRNS(Wr[2],RNS(-1)));
	  rzeczywista[i+1]=syntaxRNS(rzeczywista[i+1]);
	  urojona[i+1]=multiplyRNS(temporaryR,Wi[2]);
	  urojona[i+1]=syntaxRNS(urojona[i+1]);
  }

   for(int i=0;i<2;i++){

	  temporaryR=addRNS(rzeczywista[i],rzeczywista[i+2]);
	  temporaryI=addRNS(urojona[i],urojona[i+2]);
	  rzeczywista[i+2]=subRNS(rzeczywista[i],rzeczywista[i+2]);
	  urojona[i+2]=subRNS(urojona[i],urojona[i+2]);
	  rzeczywista[i]=temporaryR;
	  urojona[i]=temporaryI;
  }
  for(int i=4;i<6;i++){
	  temporaryR=addRNS(rzeczywista[i],rzeczywista[i+2]);
	  temporaryI=addRNS(urojona[i],urojona[i+2]);
	  rzeczywista[i+2]=subRNS(rzeczywista[i],rzeczywista[i+2]);
	  urojona[i+2]=subRNS(urojona[i],urojona[i+2]);
	  rzeczywista[i]=temporaryR;
	  urojona[i]=temporaryI;
  }    

   int j=0;
	   for(int i=4;i<8;i++){
	  temporaryR=rzeczywista[i];
	  rzeczywista[i]=addRNS(multiplyRNS(rzeczywista[i],Wr[j]),multiplyRNS(urojona[i],multiplyRNS(Wi[j],RNS(-1))));
	  
	  rzeczywista[i]=syntaxRNS(rzeczywista[i]);
	  urojona[i]=addRNS(multiplyRNS(urojona[i],Wr[j]),multiplyRNS(temporaryR,Wi[j]));
	 
	  urojona[i]=syntaxRNS(urojona[i]);
	  j++;
	   }

	  for(int i=0;i<4;i++){
		  temporaryR=addRNS(rzeczywista[i],rzeczywista[i+4]);
		  temporaryI=addRNS(urojona[i],urojona[i+4]);
		  rzeczywista[i+4]=subRNS(rzeczywista[i],rzeczywista[i+4]);
		  urojona[i+4]=subRNS(urojona[i],urojona[i+4]);		  
		  rzeczywista[i]=temporaryR;
		  urojona[i]=temporaryI;
	  }
	 }

};

class Sygnal{
	public:
	int static const dlugosc=8;
	int WynikR[dlugosc];
	int WynikI[dlugosc];
	int rzeczywista[dlugosc];
	int urojona[dlugosc];	
	int log2n_function(int dlugosc){
	int log2n=0;
	int temp=1;
	while(dlugosc>temp){
		temp=pow(2,log2n);
		log2n++;
		
	}
	return log2n-1;
	};
	int bitReverse(int x, int log2n) {
  int n = 0;
  for (int i=0; i < log2n; i++) {
    n <<= 1;
    n |= (x & 1);
    x >>= 1;
  }
  return n;
};
       
void fft5(){
	
	int Wr[4];
	int Wi[4];
	int temporaryR;
	int temporaryI;
	int WynikR[8];
	int WynikI[8];
	Wr[0]=16384;
	Wr[1]=11585;
	Wr[2]=0;
	Wr[3]=-11585;
	Wi[0]=0;
	Wi[1]=-11585;
	Wi[2]=-16384;
	Wi[3]=-11585;
	for (int i=0; i < 8; i++) {
    WynikR[i]=rzeczywista[bitReverse(i,3)];
    WynikI[i]=urojona[bitReverse(i,3)];
  }
  for(int i=0;i<8;i++)
  {
  	rzeczywista[i]=WynikR[i];
  	urojona[i]=WynikI[i];
  }

 for(int i=1;i<8;i=i+2){
	
	  temporaryR=rzeczywista[i];
	  rzeczywista[i]=rzeczywista[i]*Wr[0]+urojona[i]*Wi[0]*(-1);
	  rzeczywista[i]>>=14;

	 urojona[i]=urojona[i]*Wr[0]+temporaryR*Wi[0];
	  urojona[i]>>=14;
  }
  
  for(int i=0;i<8;i=i+2){
	  temporaryR=rzeczywista[i]+rzeczywista[i+1];
	  temporaryI=urojona[i]+urojona[i+1];
	  rzeczywista[i+1]=rzeczywista[i]-rzeczywista[i+1];
	  urojona[i+1]=urojona[i]-urojona[i+1];
	  rzeczywista[i]=temporaryR;
	  urojona[i]=temporaryI;
  }
  for(int i=2;i<8;i=i+4){
	  rzeczywista[i]=rzeczywista[i]*Wr[0];
	  rzeczywista[i]>>=14;
	  urojona[i]=urojona[i]*Wr[0];
	  urojona[i]>>=14;
	  temporaryR=rzeczywista[i+1];
	  rzeczywista[i+1]=urojona[i+1]*Wr[2]*(-1);
	  rzeczywista[i+1]>>=14;
	  urojona[i+1]=temporaryR*Wi[2];
	  urojona[i+1]>>=14;
  }

  for(int i=0;i<2;i++){

	  temporaryR=rzeczywista[i]+rzeczywista[i+2];
	  temporaryI=urojona[i]+urojona[i+2];
	  rzeczywista[i+2]=rzeczywista[i]-rzeczywista[i+2];
	  urojona[i+2]=urojona[i]-urojona[i+2];
	  rzeczywista[i]=temporaryR;
	  urojona[i]=temporaryI;
  }
  for(int i=4;i<6;i++){
	  temporaryR=rzeczywista[i]+rzeczywista[i+2];
	  temporaryI=urojona[i]+urojona[i+2];
	  rzeczywista[i+2]=rzeczywista[i]-rzeczywista[i+2];
	  urojona[i+2]=urojona[i]-urojona[i+2];
	  rzeczywista[i]=temporaryR;
	  urojona[i]=temporaryI;
  }    

	   int j=0;
	   for(int i=4;i<8;i++){
	  temporaryR=rzeczywista[i];
	  rzeczywista[i]=rzeczywista[i]*Wr[j]+urojona[i]*Wi[j]*(-1);
	  rzeczywista[i]>>=14;
	  urojona[i]=urojona[i]*Wr[j]+temporaryR*Wi[j];
	  urojona[i]>>=14;
	  j++;
	   }
	  for(int i=0;i<4;i++){
		  temporaryR=rzeczywista[i]+rzeczywista[i+4];
		  temporaryI=urojona[i]+urojona[i+4];
		  rzeczywista[i+4]=rzeczywista[i]-rzeczywista[i+4];
		  urojona[i+4]=urojona[i]-urojona[i+4];		  
		  rzeczywista[i]=temporaryR;
		  urojona[i]=temporaryI;
	  }
}
};

int main(){
	using namespace literals::chrono_literals;
	srand(time(NULL));
	int dlugosc=8;
	int WartosciSygnaluR[8];
	int WartosciSygnaluI[8];
	for(int i=0;i<dlugosc;i++){
		WartosciSygnaluR[i]=(rand()%20);
		WartosciSygnaluI[i]=(rand()%20);
	}
	Sygnal S;
	for(int i=0;i<dlugosc;i++){
		int R=WartosciSygnaluR[i];
		int I=WartosciSygnaluI[i];
	S.rzeczywista[i]=R;
	S.urojona[i]=I;
	}
	cout<<"start fft"<<endl;
	for(int i=0;i<8;i++){
		cout<<"rzeczywista= "<<S.rzeczywista[i]<<" urojona= "<<S.urojona[i]<<endl;
	}
	auto start=chrono::high_resolution_clock::now();
	S.fft5();
	auto end = chrono::high_resolution_clock::now();
	chrono::duration<float> duration = end - start;
	
	cout<<"Po FFT: "<<endl;
	for(int i=0;i<8;i++){
		cout<<"rzeczywista= "<<S.rzeczywista[i]<<" urojona= "<<S.urojona[i]<<endl;
	}
	cout<<"Czas trwania algorytmu FFT całkowitoliczbowy: "<<duration.count()*1000.00<<endl;
	SygnalRNS SRNS;
	for(int i=0;i<dlugosc;i++){
		int R=WartosciSygnaluR[i];
		int I=WartosciSygnaluI[i];
		SRNS.rzeczywista[i]=RNS(R);
		SRNS.urojona[i]=RNS(I);
	}
	cout<<"start fft RNS"<<endl;
	SRNS.readsignalRNS();
	 start=chrono::high_resolution_clock::now();
	SRNS.fft();
	end = chrono::high_resolution_clock::now();
	chrono::duration<float> durationrns = end - start;
	cout<<"PO FFT: "<<endl;
	SRNS.readsignalRNS();
	cout<<"Czas trwania algorytmu FFT RNS: "<<durationrns.count()*1000.00<<endl;

	return 0;
}